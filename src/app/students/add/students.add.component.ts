import { Component } from '@angular/core';
import Student from '../../entity/student';
import { FormBuilder } from '@angular/forms';
import { StudentService } from 'src/app/service/student-service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-students-add',
  templateUrl: './students.add.component.html',
  styleUrls: ['./students.add.component.css']
})
export class StudentsAddComponent {
  students: Student[];

  constructor(private fb: FormBuilder, private studentService: StudentService, private router: Router) {

  }
  form = this.fb.group({
    id: [''],
    studentId: [''],
    name: [''],
    surname: [''],
    gpa: [''],
    image: [''],
    featured: [''],
    penAmount: [''],
    description: ['']
  })

  submit() {
    this.studentService.saveStudent(this.form.value)
    .subscribe((student) => {
      this.router.navigate(['./detail', student.id]);
    }, (error)=> {
      alert('could not save the value')
    }
    )
  }

  get diagnostic() {
    console.log(this.form.value);
    return JSON.stringify(this.form.value)
    
  }

  upQuantity(student: Student) {
    this.form.patchValue({
      penAmount: +this.form.value['penAmount']+1 });
  }

  downQuantity(student: Student) {
    if (+this.form.value['penAmount'] > 0) {
      this.form.patchValue({
        penAmount: +this.form.value['penAmount'] - 1 })
    }
  }

}
